<?php


class Utilisateur{

    private $pseudo;
    private $motdepasse;
    private $email;
    private $image;
    private $genre;
    private $confmotdepasse;


    public function __construct($pseudo,$motdepasse, $email = "", $confmotdepasse = Null, $image="", $genre=""){
        $this->pseudo=$pseudo;
        $this->motdepasse=$motdepasse;
        $this->confmotdepasse=$confmotdepasse;
        $this->email=$email;
        $this->image=$image;
        $this->genre=$genre;
    }

    /**
     * @return mixed
     */
    public function getPseudo()
    {
        return $this->pseudo;
    }

    /**
     * @param mixed $pseudo
     */
    public function setPseudo($pseudo)
    {
        $this->pseudo = $pseudo;
    }

    /**
     * @return mixed
     */
    public function getMotdepasse()
    {
        return $this->motdepasse;
    }

    /**
     * @param mixed $motdepasse
     */
    public function setMotdepasse($motdepasse)
    {
        $this->motdepasse = $motdepasse;
    }

    /**
     * @return mixed
     */
    public function getConfmotdepasse()
    {
        return $this->confmotdepasse;
    }

    /**
     * @param mixed $confmotdepasse
     */
    public function setConfmotdepasse($confmotdepasse)
    {
        $this->confmotdepasse = $confmotdepasse;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return false|mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param false|mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return false|mixed
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * @param false|mixed $genre
     */
    public function setGenre($genre)
    {
        $this->genre = $genre;
    }
}
