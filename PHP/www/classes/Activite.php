<?php

class Activite{

    private $idActivite;
    private $nomActivite;
    private $debutActivite;
    private $finActivite;

    /**
     * Activite constructor.
     * @param $idActivite
     * @param $nomActivite
     * @param $debutActivite
     * @param $finActivite
     */
    public function __construct($idActivite, $nomActivite, $debutActivite, $finActivite)
    {
        $this->idActivite = $idActivite;
        $this->nomActivite = $nomActivite;
        $this->debutActivite = $debutActivite;
        $this->finActivite = $finActivite;
    }

    /**
     * @return mixed
     */
    public function getIdActivite()
    {
        return $this->idActivite;
    }

    /**
     * @param mixed $idActivite
     */
    public function setIdActivite($idActivite): void
    {
        $this->idActivite = $idActivite;
    }

    /**
     * @return mixed
     */
    public function getNomActivite()
    {
        return $this->nomActivite;
    }

    /**
     * @param mixed $nomActivite
     */
    public function setNomActivite($nomActivite): void
    {
        $this->nomActivite = $nomActivite;
    }

    /**
     * @return mixed
     */
    public function getDebutActivite()
    {
        return $this->debutActivite;
    }

    /**
     * @param mixed $debutActivite
     */
    public function setDebutActivite($debutActivite): void
    {
        $this->debutActivite = $debutActivite;
    }

    /**
     * @return mixed
     */
    public function getFinActivite()
    {
        return $this->finActivite;
    }

    /**
     * @param mixed $finActivite
     */
    public function setFinActivite($finActivite): void
    {
        $this->finActivite = $finActivite;
    }

}