<?php

class Champ{
    private $name;
    private $text;
    private $champMdp;

    public function __construct($name,$text,$typeEntree){
        $this->name=$name;
        $this->text=$text;
        $this->typeEntree = $typeEntree;
    }

    public function getName(){
        return  $this->name;
    }

    public function getText(){
        return  $this->text;
    }

    public function afficher()
    {
        if ($this->typeEntree == 'mdp'){
            echo "<label>".$this->text ."</label><input type='password' name=$this->name pattern='[!-ÿ^]{1,31}[^<>/ ]{1}' required><br/>";
        }
        elseif ($this->typeEntree == 'pseudo'){
            echo "<label>".$this->text ."</label><input type='text' name=$this->name pattern='[0-9A-Za-zÀ-ÿ]{1}[0-9A-Za-zÀ-ÿ-_]{0,18}[0-9A-Za-zÀ-ÿ_]{1}' required><br/>";
        }
        else{
            echo "<label>".$this->text ."</label><input type='text' name=$this->name required><br/>";
        }

    }
}