<?php

class Creneau
{
    private $hDebut;
    private $hFin;

    public function __construct($hDebut, $hFin)
    {
        $this->hDebut = $hDebut;
        $this->hFin = $hFin;
    }

    public function getHDebut()
    {
        return $this->hDebut;
    }

    public function getHFin()
    {
        return $this->hFin;
    }

    public function affichage()
    {
        return "" . $this->hDebut . "h-" . $this->hFin . "h";
    }
}