<?php
require_once "../classes/Activite.php";
require_once("../connexionBD/connexion.php");
require_once("../controlleur/bibliotheque.php");
session_start();

if ($_SERVER['REQUEST_METHOD'] == 'POST'){
    $idActivite = intval($_POST['idActivite']);
    $dateDebutActivite = new DateTime($_POST['dateActivite']);
    $dateDebutActivite->setTime($_POST['selectCreneau'], 0, 0);
    $dateFinActivite = new DateTime($_POST['dateActivite']);
    if ($dateDebutActivite->format('H:i:s') == '23:00:00'){//si l'activité commence à 23h, il faut qu'elle se termine le lendemain à 0h
        $dateFinActivite ->setTime(0,0,0);
        $dateFinActivite ->modify('+1 day');
    }
    else{
        $dateFinActivite->setTime($_POST['selectCreneau']+1, 0, 0);//+1 car un creneau ne fait qu'une heure
    }


    editerActivite(new Activite($idActivite,$_POST['nomActivite'],DATE($dateDebutActivite->format('Y-m-d H:i:s')),DATE($dateFinActivite->format('Y-m-d H:i:s'))),$_SESSION["pseudoUser"]);
    $params = "?date=".$_POST['dateActivite'];
    $url="Location: ../vue/PageAgenda.php";
    header("{$url}{$params}");
    exit;
}