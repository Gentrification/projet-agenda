<?php
require_once("../connexionBD/connexion.php");
require_once("../controlleur/bibliotheque.php");
session_start();

if ($_SERVER['REQUEST_METHOD'] == 'POST'){
    $idActivite = $_POST['idActivite'];

    supprimerActivite($idActivite);
    $params = "?date=".$_POST['dateActivite'];
    $url="Location: ../vue/PageAgenda.php";
    header("{$url}{$params}");
    exit;
}