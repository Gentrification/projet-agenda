<?php

function genererIdActivite(){
    $connexion = connect_bd();
    if ($connexion != False) {
        try {
            $sqlResult = "select max(idActivite) from ACTIVITE";
            if (!$connexion->query($sqlResult)){
                return ["Nous avons eu un problème de connexion. Veuillez réessayer ultérieurement"];
            }
            else {
                foreach ($connexion->query($sqlResult) as $row) {
                    $value = $row['max(idActivite)'];
                    if(isset($value)){
                        $connexion = null;
                        return (int)$row['max(idActivite)'] + 1;
                    }
                    else{
                        $connexion = null;
                        return 1;
                    }
                }
            }
        } catch (Exception $e) {
            $connexion = null;
            return ["Nous avons eu un problème de connexion. Veuillez réessayer ultérieurement."];
        }
    } else {
        $connexion = null;
        return ["Nous avons eu un problème de connexion. Veuillez réessayer ultérieurement."];
    }
}

function getDateDebutActivite($idActivite){
    $connexion = connect_bd();
    if ($connexion!=False) {
        try {
            $stmt = $connexion->prepare("SELECT debutActivite FROM ACTIVITE WHERE idActivite = :idActivite");
            $stmt->bindParam(":idActivite",$idActivite );
            if($stmt->execute()){

                $connexion = null;
                $res = new DateTime($stmt ->fetch()[0]);
                return $res;
            }
            $connexion = null;
            return ["Nous avons eu un problème de connexion. Veuillez réessayer ultérieurement."];

        } catch (Exception $e) {
            $connexion = null;
            return ["Nous avons eu un problème de connexion. Veuillez réessayer ultérieurement."];
        }
    } else {
        $connexion = null;
        return ["Nous avons eu un problème de connexion. Veuillez réessayer ultérieurement."];
    }
}

function ajouterActivite($activite,$pseudoUtilisateur){
    $activite->setNomActivite(filter_var($activite->getNomActivite(), FILTER_SANITIZE_STRING));
    $dicoAttributs = array("NomActivite" => $activite->getNomActivite());
    $verificationAttributs = verificationAttributs($dicoAttributs);
    if ($verificationAttributs == "Succes") {
        $connexion = connect_bd();
        if ($connexion) {
            try {
                $stmt = $connexion->prepare("INSERT INTO ACTIVITE(idActivite, nomActivite, debutActivite, finActivite, pseudoUtilisateur)
            VALUES (:idActivite, :nomActivite, :debutActivite, :finActivite, :pseudoUtilisateur)");
                if(
                $stmt->bindValue(":idActivite", $activite->getIdActivite()) &&
                $stmt->bindValue(":nomActivite", $activite->getNomActivite()) &&
                $stmt->bindValue(":debutActivite", $activite->getDebutActivite()) &&
                $stmt->bindValue(":finActivite", $activite->getFinActivite()) &&
                $stmt->bindValue(":pseudoUtilisateur", $pseudoUtilisateur) &&
                $stmt->execute()){
                    $connexion = null;
                    return ["Succes"];
                }
                $connexion = null;
                return ["Nous avons eu un problème de connexion. Veuillez réessayer ultérieurement."];

            } catch (Exception $e) {
                $connexion = null;
                return ["Nous avons eu un problème de connexion. Veuillez réessayer ultérieurement."];
            }
        } else {
            $connexion = null;
            return ["Nous avons eu un problème de connexion. Veuillez réessayer ultérieurement."];
        }
    }
}

function supprimerActivite($idActivite){
    $connexion = connect_bd();
    if ($connexion!=False) {
        try {
            $stmt = $connexion->prepare("DELETE FROM `activite` WHERE idActivite = :idActivite");
            $stmt->bindParam(":idActivite",$idActivite );
            if($stmt->execute()){
                $connexion = null;
                return ["Succes"];
            }
            $connexion = null;
            return ["Nous avons eu un problème de connexion. Veuillez réessayer ultérieurement."];

        } catch (Exception $e) {
            $connexion = null;
            return ["Nous avons eu un problème de connexion. Veuillez réessayer ultérieurement."];
        }
    } else {
        $connexion = null;
        return ["Nous avons eu un problème de connexion. Veuillez réessayer ultérieurement."];
    }
}

function editerActivite($activite,$pseudoUtilisateur){
    $dicoAttributs = array("NomActivite" => $activite->getNomActivite());
    $verificationAttributs = verificationAttributs($dicoAttributs);
    if ($verificationAttributs == "Succes") {
        $connexion = connect_bd();
        if ($connexion!=False) {
            try {
                $stmt = $connexion->prepare("UPDATE ACTIVITE SET nomActivite=:nomActivite, debutActivite=:debutActivite, finActivite=:finActivite, pseudoUtilisateur=:pseudoUtilisateur WHERE idActivite=:idActivite");
                $stmt->bindValue(":idActivite", $activite->getIdActivite());
                $stmt->bindValue(":nomActivite", $activite->getNomActivite());
                $stmt->bindValue(":debutActivite", $activite->getDebutActivite());
                $stmt->bindValue(":finActivite", $activite->getFinActivite());
                $stmt->bindValue(":pseudoUtilisateur", $pseudoUtilisateur);
                if($stmt->execute()){
                    $connexion = null;
                    return ["Succes"];
                }
                $connexion = null;
                return ["Nous avons eu un problème de connexion. Veuillez réessayer ultérieurement."];

            } catch (Exception $e) {
                $connexion = null;
                return ["Nous avons eu un problème de connexion. Veuillez réessayer ultérieurement."];
            }
        } else {
            $connexion = null;
            return ["Nous avons eu un problème de connexion. Veuillez réessayer ultérieurement."];
        }
    }
}

function ajouterUtilisateur($utilisateur){
    $utilisateur->setPseudo(filter_var($utilisateur->getPseudo(), FILTER_SANITIZE_STRING));
    $utilisateur->setMotdepasse(filter_var($utilisateur->getMotdepasse(), FILTER_SANITIZE_STRING));
    $utilisateur->setConfmotdepasse(filter_var($utilisateur->getConfmotdepasse(), FILTER_SANITIZE_STRING));
    $dicoAttributs = array("Pseudo"=>$utilisateur->getPseudo(), "ExistencePseudo"=>$utilisateur->getPseudo() , "Motdepasse" => $utilisateur->getMotdepasse(), "ConfMotdepasse" => [$utilisateur->getMotdepasse(), $utilisateur->getConfmotdepasse()], "Email" => $utilisateur->getEmail(), "Image" => $utilisateur->getImage(), "Genre" => $utilisateur->getGenre());
    $verificationAttributs = verificationAttributs($dicoAttributs);
    if($verificationAttributs == "Succes"){
        try{
            $connexion = connect_bd();
            if($connexion){
                $stmt = $connexion->prepare("INSERT INTO UTILISATEUR(pseudoUtilisateur, motDePasseHash, emailUtilisateur, photoUtilisateur, genreUtilisateur)
        VALUES (:pseudoUser, SHA2(CONCAT(:mdpUser, :pseudoUser), 256), :emailUser, :imageUser, :genreUser)");
                if($stmt->bindValue(":pseudoUser", $utilisateur->getPseudo()) &&
                $stmt->bindValue(":mdpUser", $utilisateur->getMotdepasse()) &&
                $stmt->bindValue(":emailUser", $utilisateur->getEmail()) &&
                $stmt->bindValue(":imageUser", $utilisateur->getImage()) &&
                $stmt->bindValue(":genreUser", $utilisateur->getGenre()) && $stmt->execute()){
                    $connexion = null;
                    return ["Succes"];
                }
                else{
                    $connexion = null;
                    return ["Nous avons eu un problème de connexion. Veuillez réessayer ultérieurement 1."];
                }

            }
            else{
                $connexion = null;
                return ["Nous avons eu un problème de connexion. Veuillez réessayer ultérieurement 2."];
            }
        }

        catch(Exception $e){
            $connexion = null;
            return ["Nous avons eu un problème de connexion. Veuillez réessayer ultérieurement 3."];
        }
    }
    else{
        $connexion = null;
        return $verificationAttributs;
    }
}


function connexionUtilisateur($utilisateur){
    $utilisateur->setPseudo(filter_var($utilisateur->getPseudo(), FILTER_SANITIZE_STRING));
    $utilisateur->setMotdepasse(filter_var($utilisateur->getMotdepasse(), FILTER_SANITIZE_STRING));
    $dicoAttributs = array("Pseudo"=>$utilisateur->getPseudo(), "ExistencePseudo2"=>$utilisateur->getPseudo(), "Motdepasse" => $utilisateur->getMotdepasse());
    $verificationAttributs = verificationAttributs($dicoAttributs);
    if($verificationAttributs == "Succes"){
        $connexionBD = connect_bd();
        if($connexionBD){
            try {
                $statementProcedure = $connexionBD->prepare("call verifierPseudoEtMdp(:pseudo,:mdp,@msgRetour,@msgSysteme)");
                $sqlResult = "select @msgRetour, @msgSysteme";
                if (
                    $statementProcedure->bindValue(':pseudo', $utilisateur->getPseudo(), PDO::PARAM_STR) &&
                    $statementProcedure->bindValue(':mdp', $utilisateur->getMotdepasse(), PDO::PARAM_STR) &&
                    $statementProcedure->execute() &&
                $messages = $connexionBD->query($sqlResult)
                ){

                    foreach ($messages as $row) {
                        return [$row['@msgRetour'], $row['@msgSysteme']];
                    }
                }
                else{
                    return ["Nous avons eu un problème de connexion. Veuillez réessayer ultérieurement"];
                }
            }
            catch(Exception $e){
                return ["Nous avons eu un problème de connexion. Veuillez réessayer ultérieurement"];
            }
        }
        else{
            return ["Nous avons eu un problème de connexion. Veuillez réessayer ultérieurement"];
        }

    }
    else{
        return $verificationAttributs;
    }
}



function verificationAttributs($listeAttribut){

    $liste = array();
    foreach($listeAttribut as $attribut => $valeurAttribut){
        if (("verification".$attribut)($valeurAttribut) != "Attribut valide"){
            foreach(("verification".$attribut)($valeurAttribut) as $erreur){
                array_push($liste, $erreur);
            }
        }
    }
    if (sizeof($liste)==0) {
        return "Succes";
    }
    else{
        return $liste;
    }
}

function verificationNomActivite($nomActivite){

    if (!(isset($pseudo) or empty($pseudo))){
        return ["Le champ nom activité est vide"];
    }

    $erreurs = array();
    if(strlen($nomActivite) > 255){
        array_push($erreurs, "La taille de l'activité est supérieur au maximum (255 caractères)");
    }
    if(sizeof($erreurs)>0){
        return $erreurs;
    }
    return "Attribut valide";
}

function verificationExistencePseudo($pseudo){
    if (!(isset($pseudo) or empty($pseudo))){
        return "Le champ pseudo est vide";
    }
    $erreurs = array();
    $connexionPseudo = connect_bd();
    $stmt = $connexionPseudo->prepare("SELECT pseudoUtilisateur from UTILISATEUR WHERE pseudoUtilisateur=:pseudoUser");
    $stmt->bindParam(":pseudoUser", $pseudo);
    $stmt->execute();

    if ($stmt->rowCount() > 0) {
        $connexionPseudo = Null;
        return ["Pseudo déjà utilisé"];
    }
    $connexionPseudo = Null;
    return "Attribut valide";
}

function verificationExistencePseudo2($pseudo){
    if (!(isset($pseudo) or empty($pseudo))){
        return "Le champ pseudo est vide";
    }
    $erreurs = array();
    $connexionPseudo = connect_bd();
    $stmt = $connexionPseudo->prepare("SELECT pseudoUtilisateur from UTILISATEUR WHERE pseudoUtilisateur=:pseudoUser");
    $stmt->bindParam(":pseudoUser", $pseudo);
    $stmt->execute();

    if ($stmt->rowCount() > 0) {
        $connexionPseudo = Null;
        return "Attribut valide";
    }
    $connexionPseudo = Null;
    return ["Le pseudo n'existe pas"];
}


function verificationPseudo($pseudo){
    if (!(isset($pseudo) or empty($pseudo))){
        return ["Le champ pseudo est vide"];
    }
    $erreurs = array();
    if(strlen($pseudo) > 20){
        array_push($erreurs, "La taille du pseudo est supérieur au maximum (20 caractères)");
    }
    if(strlen($pseudo) < 2){
        array_push($erreurs, "La taille du pseudo est inférieur au minimum (2 caractères)");
    }
    if(sizeof($erreurs)>0){
        return $erreurs;
    }
    return "Attribut valide";

}

function verificationMotdepasse($motdepasse){
    if (!(isset($pseudo) or empty($pseudo))){
        return ["Le champ mot de passe est vide"];
    }

    $erreurs = array();

    if(strlen($motdepasse) > 32){
        array_push($erreurs, "La taille du mot de passe est supérieur au maximum (32 caractères)");
    }
    if(strlen($motdepasse) < 6){
        array_push($erreurs, "La taille du mot de passe est inférieur au minimum (6 caractères)");
    }

    if(sizeof($erreurs)>0){
        return $erreurs;
    }
    return "Attribut valide";
}

function verificationConfMotdepasse($mdpEtConf){

    if (!(isset($pseudo) or empty($pseudo))){
        return ["Le champ de vérification du mot de passe est vide"];
    }

    $motdepasse = $mdpEtConf[0];
    $confMdp = $mdpEtConf[1];
    if(strcmp($motdepasse, $confMdp)!=0){
        return ["Les deux mots de passe sont différents"];
    }

    return "Attribut valide";
}

function verificationEmail($email){

    if (!(isset($pseudo) or empty($pseudo))){
        return ["Le champ email est vide"];
    }

    $erreurs = array();
    if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
        array_push($erreurs, "Le format de l'email est invalide");
    }

    if(sizeof($erreurs)>0){
        return $erreurs;
    }
    return "Attribut valide";
}

function verificationImage($image){

    return "Attribut valide";
}

function verificationGenre($genre){

    return "Attribut valide";
}

function chargerClasse($classe)
{
    require $classe . '.php'; // On inclut la classe correspondante au paramètre passé.
}

function recuperationActiviteJournee($pseudoUtilisateur,$journeeSelectionee){
    //input = pseudoUtilisateur, journée sélectionne dans l'input date au dessus du tableau
    //output = dico des activités de la journée de l'utilisateur : dico{clé : idActivite, valeur : debutActivite, finActivite, nomActivite}
    $res = array();
    $connexionActivite = connect_bd();
    $stmt = $connexionActivite->prepare("SELECT idActivite,nomActivite,debutActivite,finActivite from ACTIVITE natural join UTILISATEUR WHERE pseudoUtilisateur=:pseudoUser and date(debutActivite)=:journeeSelectionnee order by debutActivite asc");
    //la fonction date() de mariaDB permet d'extraire la date du datetime
    $stmt->bindParam(":pseudoUser", $pseudoUtilisateur);
    $stmt->bindParam(":journeeSelectionnee", $journeeSelectionee);
    $stmt->execute();
    if (!$stmt->execute()){
        return "Nous avons eu un problème de connexion. Veuillez réessayer ultérieurement";
    }

    elseif ($stmt->rowCount() <= 0) {
        $connexionActivite = Null;
        return "Aucune activité trouvée";
    }

    else{
        foreach ($stmt ->fetchAll() as $row){
            $res[$row['idActivite']] = [$row['debutActivite'],$row['finActivite'], $row['nomActivite']];
            //on ajoute un element au dictionnaire res
        }
    }
    return $res;
}