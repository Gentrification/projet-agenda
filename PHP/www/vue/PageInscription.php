<!DOCTYPE html>
<meta charset="utf-8" lang="fr">
<html lang="fr">
<head>
    <title>Inscription</title>
    <link rel="stylesheet" href="../css/styles.css">
</head>
<body>
<?php
require_once("../connexionBD/connexion.php");
require_once("../controlleur/bibliotheque.php");
require_once("../classes/Champ.php");
require_once("../classes/Utilisateur.php");

//spl_autoload_register('chargerClasse'); // On enregistre la fonction en autoload pour qu'elle soit appelée dès qu'on instanciera une classe non déclarée.


//AFFICHAGE
$champs=[new Champ("pseudo","Rentrer votre pseudo:",'pseudo'),new Champ("mdp","Rentrer votre mot de passe:",'mdp'),new Champ("confMdp","Confirmer votre mot de passe:",'mdp'),new Champ("email","Rentrer votre email:",'libre')];
$genres=["Masculin","Feminin","Autre"];
if ($_SERVER['REQUEST_METHOD'] == 'GET'){
    // On présente les questions
    echo "<fieldset>
    <legend><h1>Inscription</h1></legend><br/><br/>";
    echo "<form method='POST' action='PageInscription.php'>";
    foreach ($champs as $c){
        echo "<li>";
        $c -> afficher();
    }
    echo "<li>"."<label for='genre-select'>Choisissez votre genre:</label>";
    echo "<select name='genres' id='genre-select'>";
    foreach ($genres as $g) {
        echo "<option value='$g'><p>$g</p></option>";
    }
    echo "</select></li>";

    echo "<div id = 'boutons'><input type='submit' value='Continuer' id='boutonContinuer'></form>";
    echo "<form action='PageConnexion.php'>
    <input type='submit' value='Vous avez déjà un compte? Connectez-vous' id='boutonConnexion' />
    </form></div>";

    if(isset($_GET['erreurs'])){
        echo "<p> Vous avez une ou plusieurs erreurs à régler :</p></br><ul>";
        foreach ($_GET['erreurs'] as $e){
            echo "<li>".$e."</li>";
        }
        echo "</ul>";


    }

}
else{
    $utilisateur = new Utilisateur($_POST['pseudo'],$_POST['mdp'],$email = $_POST['email'], $confmotdepasse = $_POST['confMdp'], $image = "", $genre = $_POST["genres"]);
    $valeursRetour = ajouterUtilisateur($utilisateur);
    if($valeursRetour[0] == "Succes"){
        $url="Location: PageConnexion.php";
        header("{$url}");
        echo "Succès";
    }
    else{
        // Envoyer vers la page d'inscription avec la liste d'erreur en prime
        // foreach($valeursRetour as $valeurRetour){
        //        echo "</br>".$valeurRetour;
        if(!isset($_GET['erreurs'])){
            $params = '?erreurs[]=';
            $params .= implode('&erreurs[]=', $valeursRetour);
            $url="Location: PageInscription.php";
            header("{$url}{$params}");
            exit;
        }

    }
}
echo "</fieldset>";
?>
<aside>
    <img src="../images/Inscription.png" alt="imgInscription">
</aside>
</body>
</html>