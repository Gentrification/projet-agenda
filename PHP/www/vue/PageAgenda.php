<?php
    session_start();

?>


<!DOCTYPE html>
<meta charset="utf-8" lang="fr">
<html lang="fr">
<head>
    <title>Votre agenda</title>
    <link rel="stylesheet" href="../css/stylesAgenda.css">
</head>
<body>
<?php

require_once("../connexionBD/connexion.php");
require_once("../controlleur/bibliotheque.php");
require_once "../classes/Activite.php";
require_once "../classes/Creneau.php";


if(isset($_SESSION["pseudoUser"])) {

    $creneaux_journee = [];
    foreach (range(0, 23) as $number) {
        array_push($creneaux_journee, new Creneau($number, $number + 1));
    }
    //HEADER
    echo "<header>";
    echo "<article id='ajouterActivite'><form method='POST' action='../controlleur/fonctionAjouterActivite.php'>";
    echo "<label>Veuillez rentrer le nom de l'activite :</label><input type='text' name='nomActivite' required>";
    echo "<label>Veuillez choisir le jour :</label><input type='date' name='dateActivite' required>";
    echo "<label>Veuillez choisir le créneau horaire :</label><select name='selectCreneau'>";
    foreach ($creneaux_journee as $creneau) {
        $affichage = $creneau->affichage();
        $idCreneau = strval($creneau->getHDebut());
        echo "<option value=$idCreneau><p>$affichage</p></option>";
    }

    echo "<input type='submit' name='boutonAjouterActi' value='Ajouter l activité'></form></select>";
    echo "</article><article id='utilisateur'>";
    echo "<h3>" . $_SESSION["pseudoUser"] . "</h3>";
    echo "<form action='../connexionBD/logout.inc.php'>";
    echo "<button>Se déconnecter</button></form></article></header>";

    //MAIN

    if (isset($_GET['date']) and strlen($_GET['date']) == 10){
        $today = $_GET['date'];
    }
    elseif (isset($_POST['dateActivite'])){
        $today = $_POST['dateActivite'];
    }
    else{
        $today = date('Y-m-d');
    }



    $dicoActivite = recuperationActiviteJournee($_SESSION["pseudoUser"], $today);
    echo "<form method='POST' action='PageAgenda.php'>";
    echo "<main><section id='selectionJounrneeAffichee'><input type='date' name='dateActivite' value='$today'>";
    echo "<input type='submit' value='Valider' id='boutonContinuer'></form>";
    echo "</section>";

    $creneaux = array();
    if (gettype($dicoActivite) == "array") {
        foreach ($dicoActivite as $idActivite => $infoActivite) {
            array_push($creneaux, [new Creneau(date("H", strtotime($infoActivite[0])), date("H", strtotime($infoActivite[1]))), $infoActivite[2],$idActivite]);
        }
    }

    echo "<section id='planning'>";
    echo "<table>
        <thead>
            <tr>
                <th colspan='4'>Planning de la journée</th>
            </tr>
        </thead>
        <tbody>";
    foreach ($creneaux as $creneau) {
        $affichage = $creneau[0]->affichage();//le input de type hidden permet de recuperer l'id de l'activite de la ligne
        echo "<form method='POST' action='PageEditionActivite.php'>
            <tr>
                <td>$affichage</td>
                <td>$creneau[1]</td><input type='hidden' name='nomActivite' value='$creneau[1]'><input type='hidden' name='idActivite' value='$creneau[2]'>
                <td><input type='submit' name='boutonEditerActi' value='Editer'></td></form>
                <form method='POST' action='../controlleur/fonctionSupprimerActivite.php'><input type='hidden' name='idActivite' value='$creneau[2]'><input type='hidden' name='dateActivite' value='$today'>
                <td><input type='submit' name='boutonSupprimerActi' value='Supprimer'></td></form>
            </tr>";
    }
    echo "   </tbody>
                </table></section>";
}

