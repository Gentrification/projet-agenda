<!DOCTYPE html>
<meta charset="utf-8" lang="fr">
<html lang="fr">
<head>
    <title>Edition d'activité</title>
    <link rel="stylesheet" href="../css/stylesAgenda.css">
</head>
<body>
<?php
require_once("../connexionBD/connexion.php");
require_once("../controlleur/bibliotheque.php");
require_once "../classes/Creneau.php";
session_start();

if ($_SERVER['REQUEST_METHOD'] == 'POST'){
    $nomActivite = $_POST['nomActivite'];
    $idActivite = $_POST['idActivite'];
    $dateDebut = getDateDebutActivite($idActivite);
    $jour = $dateDebut->format('Y-m-d');
    $heureDebut = $dateDebut->format('H');

    $creneaux_journee = [];
    foreach (range(0, 23) as $number) {
        array_push($creneaux_journee, new Creneau($number, $number + 1));
    }

    echo "<article id='ajouterActivite'><form method='POST' action='../controlleur/fonctionEditerActivite.php'>";
    echo "<label>Veuillez rentrer le nom de l'activite :</label><input type='text' name='nomActivite' value='$nomActivite' required>";
    echo "<label>Veuillez choisir le jour :</label><input type='date' name='dateActivite' value='$jour' required>";
    echo "<label>Veuillez choisir le créneau horaire :</label><select name='selectCreneau'>";
    foreach ($creneaux_journee as $creneau) {
        $affichage = $creneau->affichage();
        $idCreneau = strval($creneau->getHDebut());
        if ($creneau->getHDebut() == intval($heureDebut)){
            echo "<option value=$idCreneau selected='selected'><p>$affichage</p></option>";
        }
        else{
            echo "<option value=$idCreneau><p>$affichage</p></option>";
        }
    }
    echo "<input type='hidden' name='idActivite' value='$idActivite'>";
    echo "<input type='submit' name='boutonEditerActi' value='Confirmer la modification'></form></select>";

}
?>
<a href="PageAgenda.php">Retour</a>
</body>
</html>
