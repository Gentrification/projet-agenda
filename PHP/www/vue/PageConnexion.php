<!DOCTYPE html>
<meta charset="utf-8" lang="fr">
<html lang="fr">
<head>
    <title>Connexion</title>
    <link rel="stylesheet" href="../css/styles.css">
</head>
<body>

<?php

require_once("../connexionBD/connexion.php");
require_once("../controlleur/bibliotheque.php");
require_once("../classes/Champ.php");
require_once("../classes/Utilisateur.php");

//spl_autoload_register('chargerClasse'); // On enregistre la fonction en autoload pour qu'elle soit appelée dès qu'on instanciera une classe non déclarée.


$champs=[new Champ("pseudo","Rentrer votre pseudo:",'pseudo'),new Champ("mdp","Rentrer votre mot de passe:",'mdp')];

if ($_SERVER['REQUEST_METHOD'] == 'GET'){
    // On présente les questions
    echo "<fieldset>
    <legend><h1>Connexion</h1></legend><br/><br/>";
    echo "<form method='POST' action='PageConnexion.php'>";
    foreach ($champs as $c){
        echo "<li>";
        $c -> afficher();
        echo "</li>";
    }

    echo "<div id='boutons'><input type='submit' value='Continuer' id='boutonContinuer'></form>";
    echo "<form action='PageInscription.php'>
    <input type='submit' value='Vous n&#39avez pas de compte? Creez en un' id='boutonInscription' />
    </form></div>";

    if(isset($_GET['erreurs'])){
        echo "<p> Vous avez une ou plusieurs erreurs à régler :</p></br><ul>";
        foreach ($_GET['erreurs'] as $e){
            echo "<li>".$e."</li>";
        }
        echo "</ul>";
    }
}
else{
    $utilisateur = new Utilisateur($_POST['pseudo'], $_POST['mdp']);
    $valeursRetour = connexionUtilisateur($utilisateur);
    if($valeursRetour[0] == "Succes"){
        session_start();
        $_SESSION["pseudoUser"] = $utilisateur->getPseudo();
        $url="Location: PageAgenda.php";
        header("{$url}");
        exit;
    }
    else{
        if(!isset($_GET['erreurs'])){
            $params = '?erreurs[]=';
            $params .= implode('&erreurs[]=', $valeursRetour);
            $url="Location: PageConnexion.php";
            header("{$url}{$params}");
            exit;
        }

    }
}
echo "</fieldset>";
?>
<aside>
    <img src="../images/Inscription.png" alt="imgInscription">
</aside>
</body>
</html>
