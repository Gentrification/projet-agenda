USE AGENDA;
DROP PROCEDURE IF EXISTS verifierPseudoEtMdp;


DELIMITER //

CREATE PROCEDURE verifierPseudoEtMdp(in pseudoUser nvarchar(20), in motdepasse nvarchar(32), out messageDeRetour nvarchar(64), out texte nvarchar(256))
    BEGIN
        SET @pseudo=(SELECT pseudoUtilisateur FROM UTILISATEUR WHERE pseudoUtilisateur=pseudoUser AND motdepasseHash=SHA2(CONCAT(motdepasse, pseudoUser), 256));
        IF(@pseudo IS NULL) THEN
            SET messageDeRetour='Mot de passe incorrect';
        ELSE
            SET messageDeRetour = 'Succes';
        END IF;
        GET DIAGNOSTICS condition 1 texte = MESSAGE_TEXT;
    END;
//

DELIMITER ;
