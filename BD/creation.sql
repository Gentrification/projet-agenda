CREATE DATABASE IF NOT EXISTS AGENDA DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE AGENDA;
DROP TABLE IF EXISTS ACTIVITE;
DROP TABLE IF EXISTS UTILISATEUR;


CREATE TABLE UTILISATEUR(

    pseudoUtilisateur nvarchar(20) NOT NULL,
    motDePasseHash BINARY(64) NOT NULL,
    emailUtilisateur nvarchar(320) NOT NULL,
    photoUtilisateur LONGBLOB,
    genreUtilisateur nvarchar(16),
    PRIMARY KEY (pseudoUtilisateur)

);


CREATE TABLE ACTIVITE(

    idActivite decimal(6, 0) NOT NULL,
    nomActivite varchar(255) NOT NULL,
    debutActivite datetime NOT NULL,
    finActivite datetime NOT NULL,
    pseudoUtilisateur nvarchar(20) NOT NULL,
    PRIMARY KEY (idActivite)

);

ALTER TABLE ACTIVITE ADD FOREIGN KEY (pseudoUtilisateur) REFERENCES UTILISATEUR (pseudoUtilisateur);