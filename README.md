Membres du groupe : Xavier Brun, Léonard Delêtre 2A12

Installation :  
cd ~/www  
git clone https://gitlab.com/Gentrification/projet-agenda.git  
Rentrer vos coordonnées gitlab  
Donner les droits d'accès et de lecture à "other" (de manière recursive) au dossier projet-agenda  
Créer la base de donnée en appelant les 2 fichiers .sql situés dans projet-agenda/BD/  
Modifier les informations dans projet-agenda/PHP/www/connexionBD/connect.php pour qu'elles correspondent à votre serveur et à vos identifiants  
  
Lancement :  
Aller à l'url suivant :  
http://localhost/[login]/projet-agenda/PHP/www/vue/PageInscription.php  
  
![](/PHP/www/images/mocodoMCD.png)  